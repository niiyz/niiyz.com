import React from "react"
import { graphql } from "gatsby"

export default ({ data }) => {
    const nodes = data.allMarkdownRemark.nodes;

    return (
        <div>
            <h1>niiyz.com</h1>
            {nodes.map(node => {
                return (
                    <ul key={node.id}>
                        <h2><a href={node.frontmatter.slug}>{node.frontmatter.title}</a> {node.frontmatter.date}</h2>
                    </ul>
                )
            })}
        </div>
    )
}

export const query = graphql`
query {
  allMarkdownRemark {
    nodes {
      frontmatter {
        title
        slug
        date(locale: "")
      }
      id
    }
  }
}
`